package com.example.examen_unidad_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import Vector_Mg
import Vector_Result
import android.content.Context
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import java.math.RoundingMode

class problema_2_Activity : AppCompatActivity() {
    private lateinit var txt_result : TextView
    private lateinit var btn_calcule :Button
    private lateinit var txt_mgA : EditText
    private var mgA : Double = 0.0
    private lateinit var txt_mgB : EditText
    private var mgB : Double = 0.0
    private lateinit var txt_mgC : EditText
    private var mgC : Double = 0.0
    private lateinit var Edit_anguleA : EditText
    private lateinit var Edit_anguleB : EditText
    private lateinit var Edit_anguleC : EditText


    private var anguleA : Double = 0.0
    private var anguleB : Double = 0.0
    private var anguleC : Double = 0.0
    lateinit var A : Vector_Mg
    lateinit var B : Vector_Mg
    lateinit var C : Vector_Mg
    lateinit var D : Vector_Result
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problema2)
        init()
        txtChange()
        OnClick()
    }
    fun init(){
        txt_result = findViewById(R.id.txt_result)
        btn_calcule = findViewById(R.id.btn_calcule)
        txt_mgA= findViewById(R.id.txt_mgA)
        mgA = txt_mgA.text.toString().toDouble()
        txt_mgB= findViewById(R.id.txt_mgB)
        mgB = txt_mgB.text.toString().toDouble()
        txt_mgC= findViewById(R.id.txt_mgC)
        mgC= txt_mgC.text.toString().toDouble()
        Edit_anguleA= findViewById(R.id.Edit_anguleA)
        Edit_anguleB= findViewById(R.id.Edit_anguleB)
        Edit_anguleC= findViewById(R.id.Edit_anguleC)


        anguleA = Edit_anguleA.text.toString().toDouble()
        anguleB = Edit_anguleB.text.toString().toDouble()
        anguleC = Edit_anguleC.text.toString().toDouble()
        A = Vector_Mg(mgA, anguleA)
        B = Vector_Mg(mgB, anguleB)
        C = Vector_Mg(mgC, anguleC)
        D = Vector_Result(arrayOf(A,B,C))
    }

    fun txtChange(){
        A.set_mag(txt_mgA.text.toString().toDouble())
        A.set_angle(Edit_anguleA.text.toString().toDouble())
        B.set_mag(txt_mgB.text.toString().toDouble())
        B.set_angle(Edit_anguleB.text.toString().toDouble())
        C.set_mag(txt_mgC.text.toString().toDouble())
        C.set_angle(Edit_anguleC.text.toString().toDouble())
        D.setArray(arrayOf(A,B,C))
    }


    fun OnClick(){
        btn_calcule.setOnClickListener {
            if(txt_result.visibility != View.VISIBLE)
                txt_result.visibility = View.VISIBLE
            txtChange()

            txt_result.text = "Dx= ${D.sum_Fx().toBigDecimal().setScale(3, RoundingMode.UP)} N\n\n " +
                    "Dy= ${D.sum_Fy().toBigDecimal().setScale(3,RoundingMode.UP)} N" +
                    "\n\n" +"D= ${D.mag_T().toBigDecimal().setScale(3,RoundingMode.UP)} N  " +
                    "${D.angle().toBigDecimal().setScale(3,RoundingMode.UP)}°"


        }
    }


    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val ret = super.dispatchTouchEvent(ev)
        ev?.let { event ->
            if (event.action == MotionEvent.ACTION_UP) {
                currentFocus?.let { view ->
                    if (view is EditText) {
                        val touchCoordinates = IntArray(2)
                        view.getLocationOnScreen(touchCoordinates)
                        val x: Float = event.rawX + view.getLeft() - touchCoordinates[0]
                        val y: Float = event.rawY + view.getTop() - touchCoordinates[1]
                        //If the touch position is outside the EditText then we hide the keyboard
                        if (x < view.getLeft() || x >= view.getRight() || y < view.getTop() || y > view.getBottom()) {
                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(view.windowToken, 0)
                            view.clearFocus()
                        }
                    }
                }
            }
        }
        return ret
    }




}