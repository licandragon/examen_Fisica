package com.example.examen_unidad_1

import Vector
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.math.RoundingMode


class problema_3_Activity : AppCompatActivity() {
    private lateinit var v_T1_i: EditText
    private lateinit var v_T1_j: EditText
    private lateinit var v_T1_k: EditText
    private lateinit var v_T2_i: EditText
    private lateinit var v_T2_j: EditText
    private lateinit var v_T2_k: EditText
    private lateinit var v_T3_i: EditText
    private lateinit var v_T3_j: EditText
    private lateinit var v_T3_k: EditText
    private lateinit var v_pT: EditText
    private lateinit var btn_calcule: Button
    private lateinit var txt_result: TextView
    lateinit var T1: DoubleArray
    lateinit var T2:DoubleArray
    lateinit  var T3:DoubleArray
    var Ty: Double =0.0
    lateinit var aux: Array<DoubleArray>
    lateinit var det: DoubleArray
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problema3)
        v_T1_i= findViewById(R.id.v_T1_i)
        v_T1_j= findViewById(R.id.v_T1_j)
        v_T1_k= findViewById(R.id.v_T1_k)
        v_T2_i= findViewById(R.id.v_T2_i)
        v_T2_j= findViewById(R.id.v_T2_j)
        v_T2_k= findViewById(R.id.v_T2_k)
        v_T3_i= findViewById(R.id.v_T3_i)
        v_T3_j= findViewById(R.id.v_T3_j)
        v_T3_k= findViewById(R.id.v_T3_k)
        v_pT= findViewById(R.id.v_pT)
        btn_calcule=findViewById(R.id.btn_calcule)
        txt_result=findViewById(R.id.txt_result)



       txtChange()
        btn_calcule.setOnClickListener {
            if(txt_result.visibility != View.VISIBLE)
                txt_result.visibility = View.VISIBLE
            txtChange()
            calculate()
            txt_result.text="T1=${det[0].toBigDecimal().setScale(3,RoundingMode.UP)} N\n" +
                    "T2=${det[1].toBigDecimal().setScale(3,RoundingMode.UP)} N\n" +
                    "T3=${det[2].toBigDecimal().setScale(3,RoundingMode.UP)} N"
        }


    }
    private fun txtChange(){
        T1 = Vector(v_T1_i.text.toString().toDouble(),v_T1_j.text.toString().toDouble(),v_T1_k.text.toString().toDouble()).unitario().toDoubleArray()
        T2= Vector(v_T2_i.text.toString().toDouble(),v_T2_j.text.toString().toDouble(),v_T2_k.text.toString().toDouble()).unitario().toDoubleArray()
        T3= Vector(v_T3_i.text.toString().toDouble(),v_T3_j.text.toString().toDouble(),v_T3_k.text.toString().toDouble()).unitario().toDoubleArray()
        Ty= v_pT.text.toString().toDouble()
    }

    private fun calculate(){
        aux= arrayOf(T1,T2,T3)
        det= Cramer.methodOfCramer(aux, doubleArrayOf(0.0,Ty,0.0))
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val ret = super.dispatchTouchEvent(ev)
        ev?.let { event ->
            if (event.action == MotionEvent.ACTION_UP) {
                currentFocus?.let { view ->
                    if (view is EditText) {
                        val touchCoordinates = IntArray(2)
                        view.getLocationOnScreen(touchCoordinates)
                        val x: Float = event.rawX + view.getLeft() - touchCoordinates[0]
                        val y: Float = event.rawY + view.getTop() - touchCoordinates[1]
                        //If the touch position is outside the EditText then we hide the keyboard
                        if (x < view.getLeft() || x >= view.getRight() || y < view.getTop() || y > view.getBottom()) {
                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(view.windowToken, 0)
                            view.clearFocus()
                        }
                    }
                }
            }
        }
        return ret
    }

}