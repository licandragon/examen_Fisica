import kotlin.math.*
class Vector(var i:Double, var j: Double, var k: Double) {
    fun magnitud() : Double{
        val mag = sqrt(i.pow(2) +j.pow(2)+k.pow(2))
        return mag
    }
    fun unitario(): Array<Double>{
        var mag = magnitud()
        var x: Double
        var y: Double
        var z : Double
        println("Valor de i= ${i}")
        println("Valor de j= ${j}")
        println("Valor de k= ${k}")
        x= i/mag
        y= j/mag
        z= k/mag
        return arrayOf(x,y,z)
    }

}

class Vector_Mg(mag: Double, angle: Double){
    private var mag: Double = mag
    private var angle: Double = angle
    fun comp_y(): Double{
        return mag * sin(Math.toRadians(angle))
    }
    fun comp_x(): Double{
        return mag * cos(Math.toRadians(angle))
    }
    fun get_mag(): Double{
        return mag
    }
    fun get_angle(): Double{
        return angle
    }
    fun set_mag(m: Double){
        mag=m
    }
    fun set_angle(a: Double){
        angle=a
    }

}

class Vector_Result(var vects: Array<Vector_Mg>){
    fun setArray(a:Array<Vector_Mg>){
        vects=a
    }
    fun sum_Fy(): Double{
        var Fy: Double = 0.0
        for(i in vects.indices){
            Fy+= vects[i].comp_y()
        }
        return Fy
    }

    fun sum_Fx(): Double{
        var Fx: Double = 0.0
        for(i in vects.indices){
           // println(vects[i].comp_x())
            Fx+= vects[i].comp_x()
        }
        return Fx
    }

    fun angle(): Double{
        return Math.toDegrees(atan(sum_Fy()/sum_Fx()))
    }

    fun mag_T(): Double{
        return sqrt(sum_Fx().pow(2)+ sum_Fy().pow(2))
    }
}

