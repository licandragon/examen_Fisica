package com.example.examen_unidad_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import Vector_Mg
import Vector_Result
import android.content.Context
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn_1= findViewById<Button>(R.id.btn_problem_1)
        val btn_2= findViewById<Button>(R.id.btn_problem_2)
        val btn_3= findViewById<Button>(R.id.btn_problem_3)

    }

    fun onClick(v: View){
        var id=v.id
        var intent: Intent
        if (id==R.id.btn_problem_1) {
            intent = Intent(this, problema_1_Activity::class.java)
            startActivity(intent)
        }
        if (id==R.id.btn_problem_2) {
             println(id)
             var i:Intent = Intent(this, problema_2_Activity::class.java)

             startActivity(i)
         }
         if (id==R.id.btn_problem_3) {

             intent = Intent(this, problema_3_Activity::class.java)

             startActivity(intent)
         }
    }
}

