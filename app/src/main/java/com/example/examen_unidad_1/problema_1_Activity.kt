package com.example.examen_unidad_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import Vector_Mg
import android.content.Context
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import java.math.RoundingMode

class problema_1_Activity : AppCompatActivity() {
    private lateinit var  txt_mag: EditText
    private var  magnitud: Double = 0.0
    private lateinit var  txt_angule: EditText
    private var angulo: Double = 0.0
    private lateinit var  btn_calcule: Button
    private lateinit var  txt_result: TextView
    private lateinit var  a: Vector_Mg
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problema1)
        txt_mag = findViewById(R.id.txt_magnitud)
        magnitud =txt_mag.text.toString().toDouble()
        txt_angule= findViewById(R.id.txt_angulo)
        angulo =txt_angule.text.toString().toDouble()
        btn_calcule = findViewById(R.id.btn_calcule)
        txt_result = findViewById(R.id.txt_result)
        a = Vector_Mg(magnitud,angulo)

        txtChange()
        btn_calcule.setOnClickListener {
            if(txt_result.visibility != View.VISIBLE)
                txt_result.visibility = View.VISIBLE
            txtChange()
            a.set_angle(angulo)
            a.set_mag(magnitud)

            txt_result.text = "Ax= ${a.comp_x().toBigDecimal().setScale(3, RoundingMode.UP)} N\n\n" +
                    "Ay= ${a.comp_y().toBigDecimal().setScale(3,RoundingMode.UP)} N"

        }

    }
    fun txtChange(){
        angulo =txt_angule.text.toString().toDouble()
        magnitud =txt_mag.text.toString().toDouble()
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val ret = super.dispatchTouchEvent(ev)
        ev?.let { event ->
            if (event.action == MotionEvent.ACTION_UP) {
                currentFocus?.let { view ->
                    if (view is EditText) {
                        val touchCoordinates = IntArray(2)
                        view.getLocationOnScreen(touchCoordinates)
                        val x: Float = event.rawX + view.getLeft() - touchCoordinates[0]
                        val y: Float = event.rawY + view.getTop() - touchCoordinates[1]
                        //If the touch position is outside the EditText then we hide the keyboard
                        if (x < view.getLeft() || x >= view.getRight() || y < view.getTop() || y > view.getBottom()) {
                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(view.windowToken, 0)
                            view.clearFocus()
                        }
                    }
                }
            }
        }
        return ret
    }



}
