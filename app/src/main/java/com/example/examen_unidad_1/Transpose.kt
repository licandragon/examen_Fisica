package com.example.examen_unidad_1

/*
    Object es lo equivalente a una clase estatica en Java
    Este object nos calcular la transpuesta de una matriz de nxn
 */
object Transpose {
    fun transponseOfMatrix(matrix:Array<DoubleArray>): Array<DoubleArray>{
        var trans: Array<DoubleArray> = Array(matrix.size){ DoubleArray(matrix.size) }
        for (i in 0 until  matrix.size)
            for (j in 0 until matrix.size)
                trans[j][i]=matrix[i][j]
        return trans
    }

}