package com.example.examen_unidad_1
/*
    Object es lo equivalente a una clase estatica en Java
    Resuelve un sistema por metodo de cramer
 */
object Cramer {
    fun methodOfCramer(matrix: Array<DoubleArray>, coef:DoubleArray):DoubleArray{
        var temp: Array<DoubleArray> = Array(coef.size) { DoubleArray(coef.size) }
        var values: DoubleArray = DoubleArray(coef.size)
        var matrix_det: Double=Determinant.determinatOfMatrix(matrix)
        println("Coef= ${coef.contentToString()}")
        for (i in 0 until coef.size){
            cloneMatrix(matrix,temp)
            println("matrix= ${matrix.contentDeepToString()}\n")
            for (j in 0 until coef.size){

                temp[j][i]=coef[j]
                //println("[new temp[${j}][${i}]=${temp[j][i]}")
               // println("[matrix=${matrix.contentDeepToString()}")
            }

           //println(temp.contentDeepToString())
            values[i]=Determinant.determinatOfMatrix(temp)/matrix_det
           // println("coef[${i}]=${values[i]}")
        }
        return values
    }

    /*
        Funcion que copia los valores de la matriz en otra
     */
    fun cloneMatrix(v: Array<DoubleArray>,r:Array<DoubleArray>) {
        for (i in v.indices)
            r[i]=v[i].copyOf()
    }
}