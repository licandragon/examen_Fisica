package com.example.examen_unidad_1

import kotlin.math.pow
/*
    Object es lo equivalente a una clase estatica en Java
    Este object nos permite calcular el determinante de una matriz de nxn
 */
object Determinant {

    fun determinatOfMatrix(matriz: Array<DoubleArray>): Double {
        var det = 0.0
        if (matriz.size == 1) {
            return matriz[0][0]
        } else {
            for (j in matriz.indices) {
                det = det + matriz[0][j] * getCofactor(matriz, 0, j)
            }
        }
        return det
    }

    fun getCofactor(matriz: Array<DoubleArray>, fila: Int, columna: Int): Double {
        val n = matriz.size - 1
        val submatriz: Array<DoubleArray> = Array(n) { DoubleArray(n) }
        var x = 0
        var y = 0
        for (i in matriz.indices) {
            for (j in matriz.indices) {
                if (i != fila && j != columna) {
                    submatriz[x][y] = matriz[i][j]
                    y++
                    if (y >= n) {
                        x++
                        y = 0
                    }
                }
            }
        }
        return Math.pow(-1.0, (fila + columna).toDouble()).toInt() * determinatOfMatrix(submatriz)
    }
}